import 'package:demo_work/singleAlbumDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SingleAlbum extends StatelessWidget {

  final albumPhoto;
  final albumName;
  final albumSong;
  final albumReleaseDate;
  final albumDetails;

  SingleAlbum({
    this.albumPhoto,
    this.albumName,
    this.albumSong,
    this.albumReleaseDate,
    this.albumDetails
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context)=>SingleAlbumDetails(
                    singleAlbumPhoto: albumPhoto,
                    singleAlbumName: albumName,
                    singleAlbumSong: albumSong,
                    singleAlbumReleaseDate: albumReleaseDate,
                    singleAlbumDetails: albumDetails
                  )
              )
          );
        },
        child: GridTile(
          footer: Container(
            color: Colors.black,
            height: 60.0,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                  child: Text(albumName+' - '+albumSong,
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(albumReleaseDate,
                          style: TextStyle(color: Colors.white, fontSize: 10.0)
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          child: Image.asset(
            albumPhoto,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
