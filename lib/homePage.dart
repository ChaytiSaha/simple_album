import 'package:demo_work/database.dart';
import 'package:demo_work/singleAlbum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List albumList = AlbumList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Center(child: Text('NEW HIP HOP SONGS (VIDEOS)', style: TextStyle(color: Colors.orange),)),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 5.0, right: 5.0),
          child: GridView.builder(
              itemCount: albumList.length,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, childAspectRatio: (.21/.4)),
              itemBuilder: (BuildContext context, int index){
                return SingleAlbum(
                  albumPhoto: albumList[index]['albumPhoto'],
                  albumName: albumList[index]['albumName'],
                  albumSong: albumList[index]['albumSong'],
                  albumReleaseDate:albumList[index]['albumReleaseDate'],
                  albumDetails:albumList[index]['albumDetails']
                );
              }
          ),
        )
      ),
    );
  }
}
