List<dynamic> AlbumList() {
  return [
    {
      'albumPhoto': 'assets/images/1.png',
      'albumName': 'JAY',
      'albumSong':'Z Featuring',
      'albumReleaseDate':'08-01-1999',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/2.jpg',
      'albumName': 'DJ NOIZE',
      'albumSong':'TRAP TAPE',
      'albumReleaseDate':'18-03-2000',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/3.jpg',
      'albumName': 'THUG SOCH',
      'albumSong':'FLEX',
      'albumReleaseDate':'12-01-2020',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/4.jpg',
      'albumName': 'Chill Hope',
      'albumSong':'SUMMER 2020',
      'albumReleaseDate':'07-09-2020',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/5.jpg',
      'albumName': 'WACKHO\'S',
      'albumSong':'LABORATORY',
      'albumReleaseDate':'31-05-2009',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/6.jpg',
      'albumName': 'HOME & HIP HOP',
      'albumSong':'tunes live',
      'albumReleaseDate':'28-07-2015',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/7.jpg',
      'albumName': 'JAY JOHAAR',
      'albumSong':'CHATTISGHAR',
      'albumReleaseDate':'31-10-2020',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/8.jpg',
      'albumName': 'HAVE YOU',
      'albumSong':'FALLING',
      'albumReleaseDate':'08-01-1999',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

    {
      'albumPhoto': 'assets/images/9.jpg',
      'albumName': 'PRAYER',
      'albumSong':'JAKE HILL',
      'albumReleaseDate':'06-12-2009',
      'albumDetails':
      'So, Give Me 6 by E-40 is a new hip hop song that gives you a reminder that the virus, '
          'Covid-19 still exists for which pandemic situation is still going around the world.\n\n'
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum '
          'has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer '
          'took a galley of type and scrambled it to make a type specimen book. It has survived not '
          'only five centuries, but also the leap into electronic typesetting, remaining essentially '
          'unchanged. It was popularised in the 1960s with the release of Letraset sheets containing '
          'Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker '
          'including versions of Lorem Ipsum.\n\n'
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of '
          'classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a '
          'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin '
          'words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical '
          'literature, discovered the undoubtable source.\n\n'
          'There are many variations of passages of Lorem Ipsum available, but the majority have suffered '
          'alteration in some form, by injected humour, or randomised words which don\'t look even slightly '
          'believable.'
    },

  ];
}