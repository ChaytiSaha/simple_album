import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SingleAlbumDetails extends StatefulWidget {

  final singleAlbumPhoto;
  final singleAlbumName;
  final singleAlbumSong;
  final singleAlbumReleaseDate;
  final singleAlbumDetails;

  SingleAlbumDetails({
    this.singleAlbumPhoto,
    this.singleAlbumName,
    this.singleAlbumSong,
    this.singleAlbumReleaseDate,
    this.singleAlbumDetails
  });

  @override
  _SingleAlbumDetailsState createState() => _SingleAlbumDetailsState();
}

class _SingleAlbumDetailsState extends State<SingleAlbumDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[800],
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(7.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:8.0),
                    child: Container(
                      height: 230.0,
                      width: 140.0,
                      child: Image.asset(widget.singleAlbumPhoto,fit: BoxFit.cover,),
                    ),
                  ),
              SizedBox(
                width: 12.0,
              ),
              Expanded(
                flex: 6,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.singleAlbumName,
                          style: TextStyle(color: Colors.white,fontSize: 16.0,height: 1.5),
                        ),
                        Text(widget.singleAlbumName+' - '+widget.singleAlbumSong,
                          style: TextStyle(color: Colors.white70,fontSize: 12.0,height: 2.0),
                        ),
                        Text(widget.singleAlbumReleaseDate,
                          style: TextStyle(color: Colors.white70,fontSize: 12.0,height: 2.0),
                        ),
                      ],
                    ),
                ),
              )
                ],
              ),
              SizedBox(height: 10.0,),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Card(
                  color: Colors.grey[700],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                          onPressed: (){},
                          icon: FaIcon(FontAwesomeIcons.facebook,color: Colors.white,)
                      ),
                      IconButton(
                          onPressed: (){},
                          icon: FaIcon(FontAwesomeIcons.instagram,color: Colors.white,)
                      ),
                      IconButton(
                          onPressed: (){},
                          icon: FaIcon(FontAwesomeIcons.twitter,color: Colors.white,)
                      ),
                      IconButton(
                          onPressed: (){},
                          icon: FaIcon(FontAwesomeIcons.shareAlt,color: Colors.white,)
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 450.0,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    physics: ClampingScrollPhysics(),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(widget.singleAlbumSong + ' || ' + 'NEW HIP HOP SONGS (VIDEOS)\n',
                            style: TextStyle(fontWeight: FontWeight.bold,
                                color: Colors.white),),
                          ],
                        ),
                        Text(widget.singleAlbumDetails,textAlign: TextAlign.justify,
                            style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
